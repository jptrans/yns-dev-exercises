SELECT *,
CASE
 WHEN pos.name = 'CEO' THEN 'Chief Executive Officer'
 WHEN pos.name = 'CTO' THEN 'Chief Technical Officer'
 WHEN pos.name = 'CFO' THEN 'Chief Financial Officer'
 ELSE pos.name
END AS full_position_name
FROM employees emp
INNER JOIN employee_positions emp_pos on emp.id = emp_pos.employee_id
INNER JOIN positions pos on pos.id = emp_pos.position_id