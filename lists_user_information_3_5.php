<?php 
    include_once 'custom/conn.php';
    session_start();
    if (isset($_SESSION['user'])) {
?>
<!DOCTYPE html>
<html>
<head>
  <title>A simple system divided into small parts.</title>
</head>
<body>

  <button onclick="window.location.href='3_5.php'">Add User</button>
  <button onclick="window.location.href='log_out_3_5.php'">Log Out</button>

  <h1>Users Information List</h1>
  <table border="1px">
    <th>#</th>
    <th>Picture</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Email</th>
    <th>Mobile Number</th>
    <tbody>
        <?php

            if (isset($_GET['page'])) {
                $prePage = $_GET['page'];
                $pagination = $_GET['page'];

                if ($pagination > 1) {
                    $pagination = $pagination * 10 - 9;
                }
            } else {
                $pagination = 1;
                $prePage = 1;
            }

            $listsOfUsers = array();

            $sql = "SELECT * FROM user_information";
            $result = $conn -> query($sql);
            $rowCount = $result -> num_rows;

            if ($rowCount > 0) {

                if ($prePage * 10 > $rowCount) {
                    $numberOfRow = $rowCount % 10;
                } else {
                    $numberOfRow = 10;
                }

                $a = 0;
                while ($row = $result -> fetch_array(MYSQLI_NUM)) {
                    $a++;
                    $listsOfUsers[$a][] = $a;
                    $listsOfUsers[$a][] = $row[6];
                    $listsOfUsers[$a][] = $row[1];
                    $listsOfUsers[$a][] = $row[2];
                    $listsOfUsers[$a][] = $row[3];
                    $listsOfUsers[$a][] = $row[4];
                    $listsOfUsers[$a][] = $row[5];
                }

                for ($i = $pagination; $i < $pagination + $numberOfRow; $i++) {
        ?>
                    <tr>
                      <td><?=$listsOfUsers[$i][0]?></td>
                      <td><img height="50" width="50" src="images/<?=$listsOfUsers[$i][1]?>"></td>

            <?php   for ($x = 2; $x < 6; $x++) { ?>
                        <td><?=$listsOfUsers[$i][$x]?></td>
            <?php   } ?>

                    </tr>
        <?php
                }

            } else {
                echo "Nothing to show here.";
            }
        ?>
    </tbody>
    <tfoot></tfoot>
  </table>

    <?php
        $numberOfPages = ceil($rowCount / 10);

        $i = 1;
        while ($i <= $numberOfPages && $rowCount > 10) {
    ?>
            <a href="lists_user_information_3_5.php?page=<?=$i?>"><?=$i?></a>
    <?php
            $i++;
        }

        if ($rowCount === 0) {
            echo "The user information sheet is currently empty.";
        }
    ?>
</body>
<?php
} else {
    header('Location: ./3_5.php');
}
?>
</html>