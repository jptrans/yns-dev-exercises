<?php

    session_start();

    if (isset($_POST['email']) && isset($_POST['password'])) {
        $email = $_POST['email'];
        $password = $_POST['password'];
        $login = false;

        if (($file = fopen('user_information.csv', 'r')) !== false) {
            while (($data = fgetcsv($file, 1000, ",")) !== false) {
                 if($data[2] == $email && $data[5] == $password){
                     $login = true;
                     break;
                 }
            }

            fclose($file);

            if ($login) {
                $_SESSION['user'] = $email;
                header('Location: 1-6_to_1-13.php');
                //echo '<script>window.location.href="1-6_to_1-13.php"</script>';
            } else {
                echo "Incorrect or invalid login credintials. <br><a href='1-6_to_1-13.php'>Click here to go back to Login.</a>";
            }
            
        }
    } else {
        header('Location: 1-6_to_1-13.php');
    }


?>