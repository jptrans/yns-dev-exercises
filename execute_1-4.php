<?php

if (isset($_POST['execute'])) {
    if (isset($_POST['num1'])) {
        $num1 = $_POST['num1'];
        $message = '';
        for ($i = 1; $i <= $num1; $i ++) {
            if ($i % 3 === 0 && $i % 5 === 0) {
                $result = 'FizzBuzz';
            } elseif ($i % 3 === 0) {
                $result = 'Fizz';
            } elseif ($i % 5 === 0) {
                $result = 'Buzz';
            } else {
                $result = $i;
            }
            if ($i === $num1) {
                $message .= $result;
            } else {
                $message .= $result . ', ';
            }    
        }
        echo $message . ' ';
?>
        <a href='1-4.php'>Click here to try again.</a>
<?php
    }
}

?>
