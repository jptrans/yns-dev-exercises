<?php
include_once 'conn_quiz.php';

if (isset($_POST)) {
    
    $score = 0;
    $answered = array();

    foreach ($_POST as $key => $value) {
        $pos = strpos($key, 'choice_');

        if ($pos === 0) {
            $answered[] = str_replace('choice_', '', $key);
        }
    }

    for ($i = 0; $i < count($answered); $i++) {
        $id = $answered[$i];
        $answer = $_POST['choice_'.$id];

        $sql = "SELECT id, answer FROM questioners where id = '$id' AND answer = '$answer' AND status = 1";
        $result = $conn->query($sql);
        
        if ($result->num_rows == 1) {
            $score++;
        }
    }

    echo json_encode($score);

} else {
    echo "weak";
}

?>