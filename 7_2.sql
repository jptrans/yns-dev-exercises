CREATE DATABASE work;

DROP TABLE IF EXISTS `therapists`;
CREATE TABLE IF NOT EXISTS `therapists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `daily_work_shifts`;
CREATE TABLE IF NOT EXISTS `daily_work_shifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `therapist_id` varchar(255) NOT NULL,
  `target_date` date NOT NULL,
  `start_time` time,
  `end_time` time,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;


INSERT INTO therapists (id,name) VALUES (1,'John');
INSERT INTO therapists (id,name) VALUES (2,'Arnold');
INSERT INTO therapists (id,name) VALUES (3,'Robert');
INSERT INTO therapists (id,name) VALUES (4,'Ervin');
INSERT INTO therapists (id,name) VALUES (5,'Smith');


INSERT INTO daily_work_shifts (id,therapist_id,target_date,start_time,end_time) VALUES (1,1,now(),'14:00','15:00');
INSERT INTO daily_work_shifts (id,therapist_id,target_date,start_time,end_time) VALUES (2,2,now(),'22:00','23:00');
INSERT INTO daily_work_shifts (id,therapist_id,target_date,start_time,end_time) VALUES (3,3,date_add(now(), interval 1 day),'00:00','01:00');
INSERT INTO daily_work_shifts (id,therapist_id,target_date,start_time,end_time) VALUES (4,4,now(),'05:00','05:30');
INSERT INTO daily_work_shifts (id,therapist_id,target_date,start_time,end_time) VALUES (5,1,now(),'21:00','21:45');
INSERT INTO daily_work_shifts (id,therapist_id,target_date,start_time,end_time) VALUES (6,5,date_add(now(), interval 1 day),'05:30','05:50');
INSERT INTO daily_work_shifts (id,therapist_id,target_date,start_time,end_time) VALUES (7,3,date_add(now(), interval 1 day),'02:00','02:30');





SELECT therapist_id AS mst_therapist_id, target_date, start_time,end_time, CASE WHEN start_time BETWEEN '00:00:00' AND '05:59:59' THEN CONCAT(date_add(target_date, INTERVAL 1 day),' ' ,start_time) ELSE CONCAT(target_date, ' ', start_time) END AS sort_start_time FROM daily_work_shifts INNER JOIN therapists on daily_work_shifts.therapist_id = therapists.id ORDER BY target_date ASC, start_time ASC;