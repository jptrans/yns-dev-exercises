<!DOCTYPE html>
<html>
<head>
  <title>Calendar</title>
</head>
<body>
  <div>
    <button onclick="window.location.href='../5_3.php'">HOME</button>
    <button onclick="window.location.href='#'">CALENDAR</button>
    <button onclick="window.location.href='../5-1/'">QUIZ</button>
  </div>
  <h1>Calendar</h1>
  <div id="yearMonth">
    <input type="number" id="year" name="year" placeholder="Year" readonly>
    <select id="month" name="month" disabled>
      <option value="1">January</option>
      <option value="2">February</option>
      <option value="3">March</option>
      <option value="4">April</option>
      <option value="5">May</option>
      <option value="6">June</option>
      <option value="7">July</option>
      <option value="8">August</option>
      <option value="9">September</option>
      <option value="10">October</option>
      <option value="11">November</option>
      <option value="12">December</option>
    </select>
  </div>
  <div>
    <button onclick="showMonth(1)">Previous Month</button>
    <button onclick="showMonth(2)">Next Month</button>
  </div>
  <table border="1px" id="CalendarTable">
    <th>Mon</th>
    <th>Tue</th>
    <th>Wed</th>
    <th>Thu</th>
    <th>Fri</th>
    <th>Sat</th>
    <th>Sun</th>
      <tbody id="calendarBody"></tbody>
  </table>
</body>
<script src="../custom/jquery-3.6.0.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    var today = new Date();
    var year = today.getFullYear();
    var month = today.getMonth();
    $("#month option:eq("+month+")").prop('selected',true);
    $("#year").val(year);
    showMonth(0);
  });


  function showMonth(action = null){
    var year = $("#year").val();
    var month = $("#month").find(":selected").val();

    if (action > 0) {
      if (action === 1) {
        month = parseInt(month) - 1;
      } else {
        month = parseInt(month) + 1;
      }
    }
    if (month > 12) {
      month = 1;
      year = parseInt(year) + 1;
    } else if (month < 1) {
      month = 12;
      year = parseInt(year) - 1;
    }
    //alert(month + ' ' + year);
    $.ajax({
      url: 'get_days.php',
      data: {year:year,month:month},
      type: 'post',
      dataType: 'json',
      success: function(response) {
        $("#calendarBody").empty();
        var today = new Date();
        var year = today.getFullYear();
        var month = today.getMonth();
        var d = today.getDate();
        var today = year + '-' + month + '-' + d;

        var day = 1;
        for (var i = 0; i < response.totalWeek; i++) {
          $("#calendarBody").append('<tr>');
          for (var j = 0; j < 7; j++) {
           //alert(response.days[day]);
            if (day == response.days.length) {
              $("#calendarBody").append('  <td></td>');
            } else if (today == response.year +'-'+response.month+'-'+response.days[day]) {
             $("#calendarBody").append('<td style="background-color:blue">' + response.days[day++] + '</td>');
            } else {
             $("#calendarBody").append('<td>' + response.days[day++] + '</td>');
            } 
          }
          $("#calendarBody").append('</tr>');
        }
        //$("#calendarBody").html(response.data);
        $("#month option:eq("+response.month+")").prop('selected',true);
        $("#year").val(response.year);
        alert(response.totalWeek);
      }
    });
  }

</script>
</html>