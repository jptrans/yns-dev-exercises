<?php

if (isset($_POST['execute'])) {
    if (isset($_POST['inputDate'])) {
        $date = date_create($_POST['inputDate']);
        $message = '';
        for ($i = 0; $i < 3; $i++) {
            $date = date_add($date, date_interval_create_from_date_string("1 days"));
            if ($i == 2) {
                $message .= date_format($date, 'Y-m-d');
            }else {
                $message .= date_format($date, 'Y-m-d') . ', ';
            }            
        }
        echo $message . ' ';
?>
        <a href='1-5.php'>Click here to try again.</a>
<?php
    }
}

?>
