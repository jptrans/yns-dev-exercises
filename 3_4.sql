
CREATE TABLE employees (

	id int NOT NULL,
	first_name varchar(255) NOT NULL,
	last_name varchar(255) NOT NULL,
	middle_name varchar(255),
	birth_date DATE NOT NULL,
	department_id int,
	hire_date DATE,
	boss_id int,
	PRIMARY KEY (id)

);


INSERT INTO employees (id,first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id)
VALUES ('1','Manabu','Yamazaki',null,'1976-03-15','1',null,null);
INSERT INTO employees (id,first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id)
VALUES ('2','Tomohiko','Takasago',null,'1974-05-24','3','2014-04-01','1');
INSERT INTO employees (id,first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id)
VALUES ('3','Yuta','Kawakami',null,'1990-08-13','4','2014-04-01','1');
INSERT INTO employees (id,first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id)
VALUES ('4','Shogo','Kubota',null,'1985-01-31','4','2014-12-01','1');
INSERT INTO employees (id,first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id)
VALUES ('5','Lorraine','San Jose','P.','1983-10-11','2','2015-03-10','1');
INSERT INTO employees (id,first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id)
VALUES ('6','Haille','Dela Cruz','A.','1990-11-12','3','2015-02-15','2');
INSERT INTO employees (id,first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id)
VALUES ('7','Godfrey','Sarmenta','L.','1993-09-13','4','2015-01-01','1');
INSERT INTO employees (id,first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id)
VALUES ('8','Alex','Amistad','F.','1988-04-14','4','2015-04-10','1');
INSERT INTO employees (id,first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id)
VALUES ('9','Hideshi','Ogoshi',null,'1983-07-15','4','2014-06-01','1');
INSERT INTO employees (id,first_name,last_name,middle_name,birth_date,department_id,hire_date,boss_id)
VALUES ('10','Kim','','','1977-10-16','5','2015-08-06','1');


CREATE TABLE departments (

	id int NOT NULL,
	name varchar(255) NOT NULL,
	PRIMARY KEY (id)

);


INSERT INTO departments (id,name) VALUES (1,'Exective');
INSERT INTO departments (id,name) VALUES (2,'Admin');
INSERT INTO departments (id,name) VALUES (3,'Sales');
INSERT INTO departments (id,name) VALUES (4,'Development');
INSERT INTO departments (id,name) VALUES (5,'Design');
INSERT INTO departments (id,name) VALUES (6,'Marketing');

CREATE TABLE positions (

	id int NOT NULL,
	name varchar(255) NOT NULL,
	PRIMARY KEY (id)

);

INSERT INTO positions (id,name) VALUES (1,'CEO');
INSERT INTO positions (id,name) VALUES (2,'CTO');
INSERT INTO positions (id,name) VALUES (3,'CFO');
INSERT INTO positions (id,name) VALUES (4,'Manager');
INSERT INTO positions (id,name) VALUES (5,'Staff');

CREATE TABLE employee_positions (

	id int NOT NULL,
	employee_id int NOT NULL,
	position_id int NOT NULL,
	PRIMARY KEY (id)

);


INSERT INTO employee_positions (id,employee_id,position_id) VALUES (1,1,1);
INSERT INTO employee_positions (id,employee_id,position_id) VALUES (2,1,2);
INSERT INTO employee_positions (id,employee_id,position_id) VALUES (3,1,3);
INSERT INTO employee_positions (id,employee_id,position_id) VALUES (4,2,4);
INSERT INTO employee_positions (id,employee_id,position_id) VALUES (5,3,5);
INSERT INTO employee_positions (id,employee_id,position_id) VALUES (6,4,5);
INSERT INTO employee_positions (id,employee_id,position_id) VALUES (7,5,5);
INSERT INTO employee_positions (id,employee_id,position_id) VALUES (8,6,5);
INSERT INTO employee_positions (id,employee_id,position_id) VALUES (9,7,5);
INSERT INTO employee_positions (id,employee_id,position_id) VALUES (10,8,5);
INSERT INTO employee_positions (id,employee_id,position_id) VALUES (11,9,5);
INSERT INTO employee_positions (id,employee_id,position_id) VALUES (12,10,5);