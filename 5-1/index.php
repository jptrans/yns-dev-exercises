<?php
    include 'conn_quiz.php';

    $sqlQuestioners = "SELECT * FROM questioners where status = 1";
    $resultQuestioners = $conn->query($sqlQuestioners);
?>
<!DOCTYPE html>
<html>
<head>
  <title>Quiz with three multiple choices</title>
</head>
<body>
  <div>
    <button onclick="window.location.href='../5_3.php'">HOME</button>
    <button onclick="window.location.href='../5-2/'">CALENDAR</button>
    <button onclick="window.location.href='#'">QUIZ</button>
  </div>

  <h1>YNS Basic Math Quiz</h1>

  <div>
    <b>Instruction: </b> Find the value of X.
  </div>

  <form method="post" id="quizForm">
  </form>

</body>
<script src="../custom/jquery-3.6.0.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $.ajax({
      url: 'questioners.php',
      type: 'post',
      dataType: 'json',
      success: function(response) {
        var letters = ['A','B','C','D'];

        for (var i = 0; i < response.length; i++) {
          $("#quizForm").append(i + 1 + '. ' + response[i].question_detail + '<br>');

          for (var j = 0; j < 4; j++) {
            $("#quizForm").append('<label><input type="radio" name="choice_'+response[i].question_id+'" value="'+response[i].choice_id[j]+'"> ' + response[i].choice_detail[j] + '</label>');
          }

          $("#quizForm").append('<br>');
        }
  
        $("#quizForm").append('<div><button type="submit" name="submit" style="margin-top: 5px">SUBMIT</button></div>');
      }
    });

    $("#quizForm").submit(function() {
      var fd = $(this).serialize();
        $.ajax({
          url: 'check_answer.php',
          data: fd,
          type: 'post',
          dataType: 'json',
          success: function(response) {
            alert('You got ' + response + '/10.');
          }
        });
    });
  });
</script>
</html>