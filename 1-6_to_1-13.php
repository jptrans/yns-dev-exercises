<?php
    session_start();
    //session_destroy();
?>
<!DOCTYPE html>
<html>
<head>
  <title>A simple system divided into small parts.</title>
</head>
<body>

    <?php if (isset($_SESSION['user'])) { ?>

      <button onclick="window.location.href='lists_user_information_1-6_to_1-13.php'">List of Users</button>
      <button onclick="window.location.href='log_out_1-6_to_1-13.php'">Log Out</button>

      <h1>Add User Information</h1>

      <form method="post" action="execute_1-6_to_1-13.php" enctype="multipart/form-data">
        <input type="text" name="firstName" placeholder="First Name" required/><br>
        <input type="text" name="lastName" placeholder="Last Name" required/><br>
        <input type="email" name="email" placeholder="Email Address" required/><br>
        <input type="number" name="mobileNumber" placeholder="9123456789" required/><br>
        <input type="password" name="password" placeholder="Password" required/><br>
        <input type="file" name="profileImage" id="profileImage" accept="image/*" required><br><br>
        <input type="submit" name="submit">
      </form>

    <?php } else { ?>

      <h1>Login</h1>
      
      <form method="post" action="login_1-6_to_1-13.php">
        <input type="email" name="email" placeholder="Email Address">
        <input type="password" name="password" placeholder="Password"><br><br>
        <input type="submit" name="submit">
      </form>

<?php
    }
?>

</body>
</html>