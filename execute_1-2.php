<?php

if (isset($_POST['operation'])) {
    $operation = $_POST['operation'];
    if (isset($_POST['num1']) && isset($_POST['num2'])) {

        $num1 = $_POST['num1'];
        $num2 = $_POST['num2'];
        $message = 'The result is ';

        if ($operation === 'ADD') {
            $message .= $num1 + $num2;
        } elseif ($operation === 'SUBTRACT') {
            $message .= $num1 - $num2;
        } elseif ($operation === 'MULTIPLY') {
            $message .= $num1 * $num2;
        } elseif ($operation === 'DIVIDE') {
            $message .= $num1 / $num2;
        } else {
            $message = "There's an error.";
        }
        
        echo $message . ' ';
?>
        <a href='1-2.php'>Click here to try again.</a>
<?php
    }
}

?>
