<?php

    include 'conn_quiz.php';

    $sqlQuestioners = "SELECT * FROM questioners where status = 1";
    $resultQuestioners = $conn->query($sqlQuestioners);
    $outputQuestions = array();

    $i = 1;
    while ($rowQuestioner = $resultQuestioners->fetch_array()) {
        $questioner_id = $rowQuestioner[0];
        $questioner_detail = $rowQuestioner[1];

        $outputQuestions[$i]['question_id'] = $questioner_id;
        $outputQuestions[$i]['question_detail'] = $questioner_detail;

        $sqlChoices = "SELECT * FROM choices where questioner_id = '$questioner_id'";
        $resultChoices = $conn->query($sqlChoices);

        $letterChoices = array('', 'A', 'B', 'C', 'D');
        $j = 0;
        while ($rowChoice = $resultChoices->fetch_array()) {
            $choice_id = $rowChoice[0];
            $choice_detail = $rowChoice[1];
            //$outputQuestions[$i]['choice_id'][$choice_id] = $choice_id;
            $outputQuestions[$i]['choice_id'][$j] = $choice_id;
            //$outputQuestions[$i]['choice_detail'][$choice_id] = $choice_detail;
            $outputQuestions[$i]['choice_detail'][$j] = $choice_detail;
            $j++;
        }

        $i++;

    }

    $random_questions = range(1, 10);
    shuffle($outputQuestions);
    $random_questions = array_slice($outputQuestions,0,10);

    echo json_encode($random_questions);

?>