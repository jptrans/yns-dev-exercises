#!/usr/bin/env bash

sudo yum install httpd
sudo service httpd start

if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant /var/www
fi


sudo yum install mysql-server
sudo service mysqld start
sudo yum install php php-mysql

sudo chkconfig httpd on
sudo chkconfig mysqld on