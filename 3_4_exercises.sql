-- #1
SELECT * FROM employees WHERE last_name like 'K%';

-- #2
SELECT * FROM employees WHERE last_name like '%i';

-- #3
SELECT CONCAT(first_name, ' ', middle_name, ' ', last_name) AS full_name, hire_date FROM employees WHERE (hire_date BETWEEN '2015-01-01' AND '2015-03-31') ORDER BY hire_date ASC;

-- #4
SELECT emp.last_name AS Employee, boss.last_name AS Boss FROM employees emp INNER JOIN employees boss ON emp.boss_id = boss.id WHERE emp.boss_id > 0;

-- #5
SELECT last_name FROM employees emp INNER JOIN departments dept ON dept.id = emp.department_id WHERE dept.name = 'Sales' ORDER BY last_name DESC;

-- #6
SELECT COUNT(middle_name) FROM employees where CHAR_LENGTH(middle_name) > 0;

-- #7
SELECT DISTINCT UPPER(dept.name), (SELECT COUNT(*) FROM employees where department_id = dept.id) AS 'COUNT(D.id' FROM departments dept where (SELECT COUNT(*) FROM employees where department_id = dept.id) > 0;

-- #8
SELECT first_name, middle_name, last_name, hire_date FROM employees ORDER BY hire_date LIMIT 1

-- #9
SELECT UPPER(name),'' as id FROM departments dept WHERE EXISTS (SELECT * FROM employees emp WHERE department_id = dept.id) = false;

-- #10
select first_name, middle_name, last_name from employees emp, employee_positions emp_pos where emp.id = emp_pos.employee_id GROUP BY emp.id HAVING COUNT(*) > 1;