<?php
include_once 'custom/conn.php';


if (isset($_SESSION['user'])) {
    if ($_POST['submit']) {
        if (isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['email']) && isset($_POST['mobileNumber']) && isset($_POST['password']) && isset($_FILES['profileImage']['name'])) {

            //Personal Information
            $firstName = $_POST['firstName'];
            $lastName = $_POST['lastName'];
            $email = $_POST['email'];
            $mobileNumber = $_POST['mobileNumber'];
            $password = $_POST['password'];
            $imageName = 'none';
        
            $dir = "images/";
            $file = $dir . basename($_FILES['profileImage']['name']);
            $imageFileType = strtolower(pathinfo($file,PATHINFO_EXTENSION));
            $image = false;
            $upload = true;

            //Allow image only
            if ($imageFileType != 'jpg' && $imageFileType != 'png' && $imageFileType != 'jpeg' && $imageFileType != 'gif' ) {
                $upload = false;
            }

            if ($upload === true) {
                if (move_uploaded_file($_FILES['profileImage']['tmp_name'], $file)) {
                    $image = true;
                    $imageName = $_FILES['profileImage']['name'];
                }
            }

            $message = "Your data inputs: ";
            $message .= 'First Name: ' . $firstName . ', ';
            $message .= 'Last Name: ' . $lastName . ', ';
            $message .= 'Email: ' . $email . ', and ';
            $message .= 'Mobile Number: ' . $mobileNumber . ' ';

            if ($upload === false) {
                $message = "User can't be saved. Profile image must be image.";
            } else {
                $sql = "INSERT INTO user_information (first_name, last_name, email, mobile_number, password, image_path) VALUES ('$firstName','$lastName','$email','$mobileNumber','$password','$imageName')";
                if ($conn -> query($sql)) {
                    $message .= 'was saved successfully with profile image.';
                }
                $conn -> close();
            }
            echo $message;
?>
            <br><a href='3_5.php'>Click here to go back to User Information.</a>
<?php
        }
    }
} else {
    header('Location: ./3_5.php');
}

?>