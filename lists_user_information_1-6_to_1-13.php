<?php
    session_start();
    if (isset($_SESSION['user'])) {
?>

<!DOCTYPE html>
<html>
<head>
  <title>A simple system divided into small parts.</title>
</head>
<body>

  <button onclick="window.location.href='1-6_to_1-13.php'">Add User</button>
  <button onclick="window.location.href='log_out_1-6_to_1-13.php'">Log Out</button>

  <h1>Users Information List</h1>
  <table border="1px">
    <th>#</th>
    <th>Picture</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Email</th>
    <th>Mobile Number</th>
    <tbody>
        <?php
            if (isset($_GET['page'])) {
                $prePage = $_GET['page'];
                $pagination = $_GET['page'];

                if ($pagination > 1) {
                    $pagination = $pagination * 10 - 9;
                }
            } else {
                $pagination = 1;
                $prePage = 1;
            }

            $row = 0;
            $totalCol = 0;
            $listsOfUsers = array();

            if (($file = fopen('user_information.csv', 'r')) !== false) {
                while (($data = fgetcsv($file, 1000, ",")) !== false) {
                    $totalCol = count($data);
                    $row++;
                    $listsOfUsers[$row][] = $row;
                    $listsOfUsers[$row][] = $data[$totalCol-2]; // image source

                    for ($i = 0; $i < $totalCol - 2; $i++) {
                        $listsOfUsers[$row][] = $data[$i];
                    }
                }

                fclose($file);

                if ($prePage * 10 > $row) {
                    $numberOfRow = $row % 10;
                } else {
                    $numberOfRow = 10;
                }

                for ($i = $pagination; $i < $pagination + $numberOfRow; $i++) {
        ?>
                    <tr>
                      <td><?=$listsOfUsers[$i][0]?></td>
                      <td><img height="50" width="50" src="images/<?=$listsOfUsers[$i][1]?>"></td>

            <?php   for ($x = 2; $x < $totalCol; $x++) { ?>
                      <td><?=$listsOfUsers[$i][$x]?></td>
            <?php   } ?>
                    </tr>
        <?php
                }

    } else {
        echo "Nothing to show here.";
    }
        ?>
    </tbody>
    <tfoot></tfoot>
  </table>

    <?php
        $numberOfPages = ceil($row / 10);

        $i = 1;
        while ($i <= $numberOfPages && count($listsOfUsers) > 10) {
    ?>    <a href="lists_user_information_1-6_to_1-13.php?page='<?=$i?>'"><?=$i?>, </a>
    <?php
            $i++;
        }

        if ($totalCol === 0) {
            echo "The user information sheet is currently empty.";
        }
    ?>
</body>
<?php

} else {
    header('Location: ./1-6_to_1-13.php');
}
?>
</html>